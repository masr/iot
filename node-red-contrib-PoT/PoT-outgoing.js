let _ = require('lodash')
const { DEBUG, debug, parseSpec, getEnactment, getPredecessors, observe, bindings} = require('./util')

function validEmission(history, spec, preds, payload) {
  if (_.xor(spec.parameters, _.keys(payload)).length) {
    debug("parameter diff: ", _.xor(spec.parameters, _.keys(payload)))
    return false
  }

  // all in parameters must match some previously observed binding
  for (var p of spec.ins) {
    bs = history.parameters[p]
    if (!bs) {
      debug("No prior observations of parameter "+p)
      return false
    }
    if (!bs.has(payload[p])) {
      debug("in parameter " + p + " does not match any known bindings: ")
      return false
    }
  }

  for (var p of spec.parameters.filter(p => !spec.keys.includes(p))) {
    bs = bindings(p, preds)

    // not valid if any nils are bound
    if (spec.nils.includes(p) && bs.length) {
      debug("Nil parameter " + p + " is bound: ", bs, preds)
      return false
    } else if (_.some(bs, b => b != payload[p])) { //should only be one binding of p
      //this applies to both in and out parameters, which are all that remain
      debug("Parameter " + p + " does not match existing bindings: ", bs, preds)
      return false
    }
  }
  return true
}


module.exports = function(RED) {
  function BsplOutgoingNode(config) {
    RED.nodes.createNode(this,config);

    let flow = this.context().flow
    let history = flow.get("history")
    if (history == undefined) {
      history = {"parameters": {}}
      flow.set('history', history)
    }

    if (config.spec != "")
      this.messages = parseSpec(config.spec)

    var node = this;

    keys = _.union(_.flatMap(node.messages, m => m.keys))

    node.on('input', function(msg) {
      payload = _.omitBy(msg.payload, (v,k)=>k[0]=="_")
      preds = getPredecessors(keys, history, payload)

      spec = _.find(node.messages, m => validEmission(history, m, preds, payload))
      if (spec) {
        if (!_.find(preds, p => _.isEqual(_.omitBy(p,(v,k)=>k[0]=='_'), payload))) {
          // don't re-observe duplicate messages
          debug("observing: ", payload)
          observe(spec, history, payload)
        }
        msg.topic = spec.recipient.split(':')[0]
        msg.ip = spec.recipient.split(':')[0] || "localhost"
        msg.port = Number(spec.recipient.split(':')[1]) || 8000
        if (DEBUG) this.log("Sending: " + JSON.stringify(_.omit(msg, 'enactment')))
        node.send(msg);
      } else {
        if (DEBUG) this.log("Not sending: " + JSON.stringify(payload))
      }
    });
  }
  RED.nodes.registerType("PoT-outgoing",BsplOutgoingNode);
}
